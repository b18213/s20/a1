/*s20 Activity:

>> In the S20 folder, create an activity folder, an index.html file inside of it and link the index.js file.

>> Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.

a. 
	>> Create a variable number that will store the value of the number provided by the user via the prompt.

	>> Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration. 

	>> Create a condition that if the current value is less than or equal to 50, stop the loop. (use break statement)
	
	>> Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop. 
		>> use modulo operator and strict equality to check divisibility
		>> use continue statement
	
	>> Create another condition that if the current value is divisible by 5, print the number.

		>> use modulo operator and strict equality to check divisibility

b. Stretch Goals

	>> Create a variable that will contain the string supercalifragilisticexpialidocious.

	>> Create another variable with an emtpy string as a value that will store the consonants from the string.

	>> Create for Loop that will iterate through the individual letters of the string based on it’s length.

	>> Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

	>> Create an else statement that will add the letter to the second variable.
		>> use addition assignment operator
		>> use the concept of index to add the specific letter in the second variable

	>> After the loop is complete, print the filtered string without the vowels
*/

// item a.
let user_Number = Number(prompt("Enter number:"))

console.log("The number you provided is " + user_Number);

for (; user_Number >= 0; user_Number--){

	if (user_Number <= 50){
		console.log("Current value is at 50. Terminating the loop")
		break;
	};

	if (user_Number % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}else if(user_Number % 5 === 0){
		console.log(user_Number);
	}

}

